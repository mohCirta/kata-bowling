package com.sgcib.katabowling.application.rest;

import java.util.Base64;
import java.util.Base64.Decoder;

import com.sgcib.katabowling.domain.model.BowlingGame;
import com.sgcib.katabowling.domain.service.implement.PrserInputServiceImpl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class Controller {

    @GetMapping("/startGame/{rolls}")
    public ResponseEntity<Object> getresultsByStep(@PathVariable(value = "rolls") String rolls) {
        PrserInputServiceImpl poducesScore = new PrserInputServiceImpl();

        try {
            Decoder decoder = Base64.getDecoder();
            byte[] decodedByte = decoder.decode(rolls);
            String decodedString = new String(decodedByte);
            BowlingGame bowlingGame = poducesScore.prserInput(decodedString);
            bowlingGame.totalScore();
            return ResponseEntity.ok().body(bowlingGame.getResultsByStep());
        } catch (Exception e) {
            return ResponseEntity.status(500).body(e.getMessage());
        }

    }
}
