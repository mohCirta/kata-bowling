package com.sgcib.katabowling.application.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class KatabowlingException extends Exception {

  private static final long serialVersionUID = 1L;
  private static Logger LOGGER = LogManager.getLogger(KatabowlingException.class);

  public KatabowlingException(String message) {

    super(message);
    LOGGER.error(message);

  }

}
