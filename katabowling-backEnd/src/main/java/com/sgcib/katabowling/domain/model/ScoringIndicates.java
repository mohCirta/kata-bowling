package com.sgcib.katabowling.domain.model;

public enum ScoringIndicates {
    STRIKE("x"), SPARE("/"), MISS("-");
	
	final String var ;

	private ScoringIndicates(String var) {
		this.var = var;
	}

	public String getVar() {
		return var;
	}	
}
