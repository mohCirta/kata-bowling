package com.sgcib.katabowling.domain.service;


import com.sgcib.katabowling.application.exception.KatabowlingException;
import com.sgcib.katabowling.domain.model.BowlingGame;

public interface PrserInputService {

    public BowlingGame prserInput(String input) throws KatabowlingException;
    
    
}
