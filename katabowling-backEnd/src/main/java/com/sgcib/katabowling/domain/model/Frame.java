package com.sgcib.katabowling.domain.model;

import java.util.Arrays;

import com.sgcib.katabowling.domain.service.ProducesScore;

import lombok.Data;

@Data
public class Frame {

    private int[] rolls;
    private ProducesScore producesScore;

    public Frame() {
    }

    public Frame(int[] rolls, ProducesScore producesScore) {

        this.rolls = rolls;
        this.producesScore = producesScore;

    }

    public int[] getRolls() {
        return rolls;
    }

    public void setRolls(int[] rolls) {
        this.rolls = rolls;
    }

    public ProducesScore getProducesScore() {
        return producesScore;
    }

    public void setProducesScore(ProducesScore producesScore) {
        this.producesScore = producesScore;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((producesScore == null) ? 0 : producesScore.hashCode());
        result = prime * result + Arrays.hashCode(rolls);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Frame other = (Frame) obj;
        if (producesScore == null) {
            if (other.producesScore != null)
                return false;
        } else if (!producesScore.getClass().getName().equals(other.producesScore.getClass().getName()))
            return false;
        if (!Arrays.equals(rolls, other.rolls))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Frame [producesScore=" + producesScore + ", rolls=" + Arrays.toString(rolls) + "]";
    }

    
}
