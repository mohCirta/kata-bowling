package com.sgcib.katabowling.domain.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sgcib.katabowling.domain.service.ProducesScore;

import lombok.Data;

@Data
public class BowlingGame {

    public static final int MAX_FRIME = 10;
    private int[] rolls = new int[21];
    private List<Integer> resultsByStep = new ArrayList<>();
    private int currentRoll;
    private List<ProducesScore> listProducesScore;
    private int score;

    public BowlingGame() {
    }

    public BowlingGame(int[] rolls, List<ProducesScore> listProducesScore) {
        this.rolls = new int[21];
        for(int i = 0; i < rolls.length; i++ ){
            this.rolls[i] = rolls[i];
        }
        this.listProducesScore = listProducesScore;
    }

    public int totalScore() {
        this.score = 0;
        this.currentRoll = 0;
        for (int currentFrame = 0; currentFrame < MAX_FRIME; currentFrame++) {
            listProducesScore.get(currentFrame).score(this);
        }
        return this.score;
    }

 

    public int[] getRolls() {
        return rolls;
    }

    public void setRolls(int[] rolls) {
        this.rolls = rolls;
    }

    public List<Integer> getResultsByStep() {
        return resultsByStep;
    }

    public void setResultsByStep(List<Integer> resultsByStep) {
        this.resultsByStep = resultsByStep;
    }
    
    public int getCurrentRoll() {
        return currentRoll;
    }

    public void setCurrentRoll(int currentRoll) {
        this.currentRoll = currentRoll;
    }

    public List<ProducesScore> getListProducesScore() {
        return listProducesScore;
    }

    public void setListProducesScore(List<ProducesScore> listProducesScore) {
        this.listProducesScore = listProducesScore;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void addStep (int step){
        resultsByStep.add(step);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((listProducesScore == null) ? 0 : listProducesScore.hashCode());
        result = prime * result + Arrays.hashCode(rolls);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BowlingGame other = (BowlingGame) obj;
        if (listProducesScore == null) {
            if (other.listProducesScore != null)
                return false;
        } 
        else if (listProducesScore.size() == other.listProducesScore.size()){
            for(int i = 0; i < listProducesScore.size(); i++) {
                if(!listProducesScore.get(i).getClass().equals(other.listProducesScore.get(i).getClass())){
                    return false;
                }
            }
        }else {
            return false;
        }
        if(!Arrays.equals(rolls, other.rolls)){
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BowlingGame [currentRoll=" + currentRoll + ", listProducesScore=" + listProducesScore + ", rolls="
                + Arrays.toString(rolls) + ", score=" + score + "]";
    }

 

}
