package com.sgcib.katabowling.domain.service.implement;

import com.sgcib.katabowling.domain.model.BowlingGame;
import com.sgcib.katabowling.domain.service.ProducesScore;

public class ProducesScoreSpareServiceImpl implements ProducesScore {

    @Override
    public void score(BowlingGame bowlingGame) {
        
        bowlingGame.setScore(bowlingGame.getScore() + 10 + bowlingGame.getRolls()[bowlingGame.getCurrentRoll() + 2]);
        bowlingGame.addStep(bowlingGame.getScore());
        bowlingGame.setCurrentRoll(bowlingGame.getCurrentRoll() + 2);
    }

}
