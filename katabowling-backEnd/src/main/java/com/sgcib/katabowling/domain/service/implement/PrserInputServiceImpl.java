package com.sgcib.katabowling.domain.service.implement;

import java.util.ArrayList;
import java.util.List;

import com.sgcib.katabowling.application.exception.KatabowlingException;
import com.sgcib.katabowling.domain.model.BowlingGame;
import com.sgcib.katabowling.domain.model.ScoringIndicates;
import com.sgcib.katabowling.domain.service.ProducesScore;
import com.sgcib.katabowling.domain.service.PrserInputService;

public class PrserInputServiceImpl implements PrserInputService {

	private static final String SEPARATOR_ROLL = "";
	private static final String SEPARATOR_FRAME = " ";
	private static final String PATTERN_MISS = "[0-9|\\-]{2}";
	private static final String PATTERN_SPARE = ".*/.*";
	private static final String PATTERN_STRIKE = "[X|x]{1}";
	private static final String PATTERN_GENERAL = "[\\s|0-9|\\-|.*/.*|x|X]+";

	@Override
	public BowlingGame prserInput(String input) throws KatabowlingException {

		int[] rolls = new int[21];
		List<ProducesScore> listProducesScore = new ArrayList<>();
		BowlingGame bowlingGame = new BowlingGame();
		int totalPointFrame = 0;
		int currentFrame = 0;
		int currentRoll = 0;
		for (String rollsStr : validInput(input)) {
			for (String roll : rollsStr.split(SEPARATOR_ROLL)) {
				rolls[currentRoll] = convertvalueRoll(roll);
				rolls[currentRoll] = adaptationRollSpare(currentRoll, rolls);
				totalPointFrame += rolls[currentRoll];
				currentRoll++;
			}
			listProducesScore.add(selectProducesScore(rollsStr, currentFrame));
			validTotalPointFrame(totalPointFrame, currentFrame);
			totalPointFrame = 0;
			currentFrame++;
		}
		bowlingGame.setRolls(rolls);
		bowlingGame.setListProducesScore(listProducesScore);

		return bowlingGame;
	}

	private int adaptationRollSpare(int indexRoll, int[] rolls) {
		if (indexRoll > 0 && rolls[indexRoll] == -10) {
			return Math.abs(rolls[indexRoll] + rolls[indexRoll - 1]);
		}
		return rolls[indexRoll];
	}

	private int convertvalueRoll(String roll) {
		if (roll.equals(ScoringIndicates.MISS.getVar()) ) {
			return 0;
		} else if (roll.equals(ScoringIndicates.SPARE.getVar())) {
			return -10;
		} else if (roll.toLowerCase().equals(ScoringIndicates.STRIKE.getVar()) ) {
			return 10;
		} else {
			return Integer.parseInt(roll);
		}
	}

	private void validTotalPointFrame(int totalPointFrame, int currentRoll) throws KatabowlingException {
		if (totalPointFrame > 10 && currentRoll != 9) {
			throw new KatabowlingException("The Total sum of the points in the frame greater than 10!");
		}
	}

	private ProducesScore selectProducesScore(String rolls, int currentFrame) throws KatabowlingException {

		if (rolls.matches(PATTERN_STRIKE)) {

			return new ProducesScoreStrikeServiceImpl();

		} else if (rolls.matches(PATTERN_MISS) || (currentFrame > 9 && rolls.matches("[0-9]{1}"))) {

			return new ProducesScoreMissServiceImpl();

		} else if (rolls.matches(PATTERN_SPARE)
				&& ((rolls.length() == 2 && currentFrame < 9) || (rolls.length() == 3 && currentFrame == 9))) {

			return new ProducesScoreSpareServiceImpl();

		} else {
			throw new KatabowlingException("Format de la chaine etrer non respacter (selectProducesScore)!");
		}

	}

	private String[] validInput(String input) throws KatabowlingException {

		String[] scoringIndicates = input.split(SEPARATOR_FRAME);
		boolean validSuggested12Rolls = false;
		boolean validSuggested21Rolls = false;
		boolean validSuggested20Rolls = false;

		if (scoringIndicates.length >= 10 && input.matches(PATTERN_GENERAL)) {
			validSuggested20Rolls = (scoringIndicates.length == 10);
			validSuggested12Rolls = (scoringIndicates[9].matches("[x|X]?") && scoringIndicates.length == 12);
			validSuggested21Rolls = (scoringIndicates[9].matches(PATTERN_SPARE) && scoringIndicates[9].length() == 3
					&& scoringIndicates.length == 11);
		}

		if (validSuggested12Rolls || validSuggested20Rolls || validSuggested21Rolls) {
			return scoringIndicates;
		} else {
			throw new KatabowlingException(" Format de la chaine etrer non respacter !");
		}

	}
}