package com.sgcib.katabowling.domain.service;

import com.sgcib.katabowling.domain.model.BowlingGame;

public interface ProducesScore {

    public void score(final BowlingGame bowlingGame);
}
