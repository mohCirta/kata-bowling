package com.sgcib.katabowling.domain.service.implement;

import com.sgcib.katabowling.domain.model.BowlingGame;
import com.sgcib.katabowling.domain.service.ProducesScore;

public class ProducesScoreMissServiceImpl implements ProducesScore {

	@Override
	public void score(final BowlingGame bowlingGame) {

		int scoreRoll = bowlingGame.getRolls()[bowlingGame.getCurrentRoll()]
				+ bowlingGame.getRolls()[bowlingGame.getCurrentRoll() + 1];
		bowlingGame.setScore(scoreRoll + bowlingGame.getScore());
		bowlingGame.addStep(bowlingGame.getScore());
		bowlingGame.setCurrentRoll(bowlingGame.getCurrentRoll() + 2);
	}

}
