package com.sgcib.katabowling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KatabowlingApplication {

	public static void main(String[] args) {
		SpringApplication.run(KatabowlingApplication.class, args);
	}

}
