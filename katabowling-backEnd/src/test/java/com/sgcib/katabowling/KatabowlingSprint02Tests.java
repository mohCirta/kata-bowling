package com.sgcib.katabowling;

import static org.junit.jupiter.api.Assertions.assertEquals;
import com.sgcib.katabowling.application.exception.KatabowlingException;
import com.sgcib.katabowling.domain.service.implement.PrserInputServiceImpl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class KatabowlingSprint02Tests {

    PrserInputServiceImpl poducesScore = new PrserInputServiceImpl();

    @Test()
    public void prserInput20RollsErrorTest() {
        Assertions.assertAll("Input text 20 Rolls non crollsonformes",
                () -> Assertions.assertThrows(KatabowlingException.class, () -> {
                    poducesScore.prserInput("yu -1 81 -- 24 11 -2 1- 44 11");
                }), () -> Assertions.assertThrows(KatabowlingException.class, () -> {
                    poducesScore.prserInput("-1 -1 81 -- 24 11");
                }), () -> Assertions.assertThrows(KatabowlingException.class, () -> {
                    poducesScore.prserInput("9- -1 81 --- 24 11 -2 1- 44 11");
                }), () -> Assertions.assertThrows(KatabowlingException.class, () -> {
                    poducesScore.prserInput("9- -1 81 92 24 11 -2 1- 44 11");
                }), () -> Assertions.assertThrows(KatabowlingException.class, () -> {
                    poducesScore.prserInput("9- -1 81 -- 24 11 -2 1- 44 11 11");
                }));
    }

    @Test
    public void totalScore12RollsTest() throws KatabowlingException {
        Assertions.assertAll("Test function BowlingGame.totalScore() with examples for 12 Rolls",
                () -> assertEquals(300, poducesScore.prserInput("X X X X X X X X X X X X").totalScore()),
                () -> assertEquals(179, poducesScore.prserInput("5/ 45 8/ X 0/ X 62 X 4/ X X X").totalScore()),
                () -> assertEquals(186, poducesScore.prserInput("5/ 4- 81 X -/ X X X 4/ X X 5").totalScore()));
    }

    @Test
    public void totalScore21RollsTest() throws KatabowlingException {
    Assertions.assertAll("Test function BowlingGame.totalScore() with examples for 21 Rolls",
    () -> assertEquals(122, poducesScore.prserInput("81 -9 2/ X 63 7- 52 x -6 2/x").totalScore()),
    () -> assertEquals(129, poducesScore.prserInput("6/ 14 45 5/ X 01 7/ 6/ X 2/6").totalScore()),
    () -> assertEquals(150, poducesScore.prserInput("5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/5").totalScore()));
    }

}
