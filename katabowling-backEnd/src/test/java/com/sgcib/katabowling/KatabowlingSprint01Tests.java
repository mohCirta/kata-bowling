package com.sgcib.katabowling;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.sgcib.katabowling.application.exception.KatabowlingException;
import com.sgcib.katabowling.domain.model.BowlingGame;
import com.sgcib.katabowling.domain.service.ProducesScore;
import com.sgcib.katabowling.domain.service.implement.ProducesScoreMissServiceImpl;
import com.sgcib.katabowling.domain.service.implement.PrserInputServiceImpl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class KatabowlingSprint01Tests {

    PrserInputServiceImpl poducesScore = new PrserInputServiceImpl();

    private List<ProducesScore> getlistProducesScore(){
        List<ProducesScore> listProducesScore = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            listProducesScore.add(new ProducesScoreMissServiceImpl());
        }
        return listProducesScore;
    }

    @Test
    public void prserInput20RollsTrueTest() throws Exception {
        
        BowlingGame bowlingGame01 = new BowlingGame(
                new int[] { 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0 }, getlistProducesScore());

        BowlingGame bowlingGame02 = new BowlingGame(
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, getlistProducesScore());

        BowlingGame bowlingGame03 = new BowlingGame(
                new int[] { 9, 0, 0, 1, 8, 1, 0, 0, 2, 4, 1, 1, 0, 2, 1, 0, 4, 4, 1, 1 }, getlistProducesScore());

        Assertions.assertAll("Test Input Line Text For 20 Rolls ", 
            () -> assertEquals(bowlingGame01, poducesScore.prserInput("9- 9- 9- 9- 9- 9- 9- 9- 9- 9-")),
            () -> assertEquals(bowlingGame02, poducesScore.prserInput("-- -- -- -- -- -- -- -- -- --")),
            () -> assertEquals(bowlingGame03, poducesScore.prserInput("9- -1 81 -- 24 11 -2 1- 44 11")));
    }

    @Test()
    void prserInput20RollsErrorTest() {
        Assertions.assertAll("Input text 20 Rolls non crollsonformes",
            () -> Assertions.assertThrows(KatabowlingException.class, () -> {
                poducesScore.prserInput("yu -1 81 -- 24 11 -2 1- 44 11");
            }), () -> Assertions.assertThrows(KatabowlingException.class, () -> {
                poducesScore.prserInput("-1 -1 81 -- 24 11");
            }),
            () -> Assertions.assertThrows(KatabowlingException.class, () -> {
                poducesScore.prserInput("9- -1 81 --- 24 11 -2 1- 44 11");
            }),
            () -> Assertions.assertThrows(KatabowlingException.class, () -> {
                poducesScore.prserInput("9- -1 81 92 24 11 -2 1- 44 11");
            }),
            () -> Assertions.assertThrows(KatabowlingException.class, () -> {
                poducesScore.prserInput("9- -1 81 -- 24 11 -2 1- 44 11 11");
            })
        );
    }

    @Test
    public void totalScore20RollsTest() throws KatabowlingException {
        Assertions.assertAll("Test function BowlingGame.totalScore() with examples for 20 Rolls", 
        () -> assertEquals(90, poducesScore.prserInput("9- 9- 9- 9- 9- 9- 9- 9- 9- 9-").totalScore()),
        () -> assertEquals(40, poducesScore.prserInput("9- -1 81 -- 24 11 -2 1- 44 11").totalScore()), 
        () -> assertEquals(0,poducesScore.prserInput("-- -- -- -- -- -- -- -- -- --").totalScore()));
    }
}
