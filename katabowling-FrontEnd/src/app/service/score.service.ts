import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ScoreService {

  constructor(private http: HttpClient) { }

  getresultsByStept(rolls: string) : any{

    return this.http.get<any[]>('http://localhost:8080/startGame/' + btoa(rolls));
  }

}
