import {HttpEventType, HttpResponse} from '@angular/common/http';
import {ScoreService} from './service/score.service';
import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  rolls: string;
  changeImage = false;
  response: string = '';
  erroResp = null;
  affichierReu = false;



  constructor(private scoreService: ScoreService) {
    this.rolls = "";
  }


  startGame(): void {

    console.log(" this.rolls = " + this.rolls);
    this.scoreService.getresultsByStept(this.rolls).subscribe(
      rep => {
          this.response = rep;
          this.affichierReu = true;
          this.erroResp = null;

      }, error => {
        this.response = null;
        this.affichierReu = false;
        this.erroResp = error.error;
      }
    );
  }
}
